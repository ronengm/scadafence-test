const connection = require('./connection');

const getModels = require('./models');

const models = getModels(connection);

module.exports = models;