// grab the things we need
const mongoose = require('mongoose');

const Schema = mongoose.Schema;


/**
 * @module IPRangeModel
 * @author Ronen Mars <ronenmars@gmail.com>
 */
/**
 * @typedef module:IPRangeModel.IPRangeObject
 * @type {object}
 * @property {string} email Users email
 * @property {string} message Users IPRanges message
 */
// create a schema
const IPRangeschema = new Schema({
	ipStartStr: {
		type: String,
		index: true,
		sparse: true
	},
	ipStartBuffer: {
		type: Buffer,
		index: true,
		sparse: true
	},
	ipEndStr: {
		type: String,
		index: true,
		sparse: true
	},
	ipEndBuffer: {
		type: Buffer,
		index: true,
		sparse: true
	},
	isInternal: Boolean
}, {
	timestamps: true
});

// make this available to our IPRanges in our Node applications
module.exports = IPRangeschema;