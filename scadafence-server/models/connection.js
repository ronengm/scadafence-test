const mongoose = require('mongoose');

const {db_address} = require('../config');


mongoose.connect(db_address, {
	useNewUrlParser: true
})
	.then(() => console.log('INFO:','Connected to mongoDB'))
	.catch((err) => {
		console.log('CRITICAL_LOG:', `*** Can Not Connect to Mongo Server:', ${db_address}, 'Message:', ${err.message}`);
		throw new Error('Error establishing db connection!');
	});

mongoose.set('useCreateIndex', true);


(process.env.NODE_ENV == 'development') && mongoose.set('debug', true);

module.exports = mongoose;
