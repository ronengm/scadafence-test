/*eslint no-multi-spaces: ["off", { ignoreEOLIPRanges: false }]*/
const Index = {};

Index.app          = process.env.APP   || 'development';
Index.port         = process.env.PORT  || '5000';

Index.db_dialect   = process.env.DB_DIALECT    || 'mongo';
Index.db_address	= process.env.MONGO_ADDRESS;

module.exports = Index;

