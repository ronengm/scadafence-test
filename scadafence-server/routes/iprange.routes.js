const IPRangeController = require('../controllers/iprange.controller');
const express = require('express');
const router = express.Router();

// expose the routes to our app with module.exports

//----------------------------------------------------//
// --------------IPRanges Routes Public-------------------//
// ---------------------------------------------------//

router.get('/', IPRangeController.getAllIPRanges);
router.post('/', IPRangeController.addIPRange);



module.exports = router;
