const express = require('express');
var bodyParser = require('body-parser');
const IPRangeRoutes = require('./iprange.routes');

const router = express.Router();
router.use(bodyParser.json());

router.get('/', async (req, res) => {

	return res.json({
		status: 'My API is alive!',
		env: process.env.NODE_ENV
	});

})
	.use('/ipranges', IPRangeRoutes);

console.log('INFO:','Routes loaded successfully');

module.exports = router;
