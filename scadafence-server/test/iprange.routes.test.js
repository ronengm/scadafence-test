//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let IPRange = require('../models/iprange.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();


chai.use(chaiHttp);

//Our parent block
describe('IPRanges', () => {
	/*
	  * Test the /GET route
	  */
	describe('/GET IPRanges', () => {
		it('it should GET all the IPRanges', (done) => {
			chai.request(server)
				.get('/ipranges')
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					done();
				});
		});
	});

});