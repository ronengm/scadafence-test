const Joi = require('@hapi/joi'); // Validation library


/**
 * @module Controller.IPRanges
 * @version 1.0.0
 *
 * @author Ronen Mars <ronenmars@gmail.com>
 *
 * @description Scadafence controller for all ./IPRanges endpoints
 */
const models = require('../models');


/**
 * IPRanges fetcher
 * @param {Object} req Express.js request
 * @param {*} res
 */
const getAllIPRanges = async (req, res) => {


	try {
		console.log('INFO:', 'Fetching IPRanges');

		const IPRanges = await models.IPRanges.find({}).exec();
		res.json(IPRanges);


	} catch (err) {
		console.log('CRITICAL_LOG:', {
			message: err.message,
			stack: err.stack
		});

		return res
			.status(500)
			.json({
				errorMessage: `Something went wrong`,
				errorCode: `You should check it`
			});
	}
};


/**
 * Add new IPRange
 * @param {Object} req Express.js request
 * @param {*} res
 */
const addIPRange = async (req, res) => {

	try {
		console.log('INFO:', 'Adding a IPRange');

		const requestBody = req.body;

		// Accept only ipv4 and ipv6 addresses with a CIDR
		const IPScheme = Joi.string().ip({
			version: [
				'ipv4'
			]
		});

		const RequestSchema = Joi.object().keys({
			ipstart: IPScheme,
			ipend: IPScheme,
			isinternal: Joi.boolean().required()
		});

		const validation = Joi.validate(requestBody, RequestSchema);

		if (validation.error) {

			console.log('DEBUG_LOG:', `Invalid form! message: ${validation.error.details[0].message}`);
			return res
				.status(400)
				.json({
					errorMessage: validation.error.details[0].message,
					errorCode: `IPRange validation not passed successfully`
				});
		}

		const ipStartStr = requestBody.ipstart;
		const ipEndStr = requestBody.ipend;

		// Generate buffer for ip's
		const ipStartBuffer = new Buffer(requestBody.ipstart.split("."));
		const ipEndBuffer = new Buffer(requestBody.ipend.split("."));

		const isInternal = requestBody.isinternal;

		// const isValid = ipStartBuffer.compare(ipEndBuffer);

		const isExist = // check if there is collision in existing ip ranges
			await models.IPRanges.find({
				$or: [
					{
						$and: [
							{
								ipStartBuffer: {$gte: ipStartBuffer}
							},
							{ipStartBuffer: {$lte: ipEndBuffer}},
							{isInternal}

						]
					},
					{
						$and: [
							{
								ipEndBuffer: {$gte: ipStartBuffer}
							},
							{ipEndBuffer: {$lte: ipEndBuffer}},
							{isInternal}

						]
					}
				]
			});

		if (!isExist.length) {
			// add new range to the db
			await models.IPRanges.create({
				ipStartStr,
				ipStartBuffer,
				ipEndStr,
				ipEndBuffer,
				isInternal
			});

			const newIPRangesList = await models.IPRanges.find({});


			res.json(newIPRangesList);
		} else {
			console.log('IPRange already exist in the system');

			return res
				.status(500)
				.json({
					errorMessage: `IPRange already exist in the system`
				});
		}


	} catch (err) {
		console.log('CRITICAL_LOG:', {
			message: err.message,
			stack: err.stack,
			body: req.body
		});

		return res
			.status(500)
			.json({
				errorMessage: `Something went wrong`,
				errorCode: `Something crashed`
			});
	}
};


module.exports = {
	getAllIPRanges,
	addIPRange
};
