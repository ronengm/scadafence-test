# Scadefence Hometask

A test for a fullstack, using a M.E.R.N stack - The task was to develop a range management ip system.
The system stores a IP v4 ranges with mark is it internal network or not and the initial and ending IP of every range.
Collisions are forbidden.

## Backend

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

NodeJS, npm/yarn

## Running the tests

There is automated test which check if the basic '/ipranges' route works and return an array
To run the  test we would run in our terminal:
cd server
yarn test

## Deployment

In order to run the NodeJS server, we would:
add to the server .env file attached by emai, then: 
cd scadefence-server &&
yarn start

## Built With

* [mongoose] - DB Connection
* [@hapi/joi] - Validations library
* [mocha chai] - Testing

## Frontend

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

npm/yarn

## Running the tests

Frontend is tested manually - tested:
* Empty form
* Form with invalid ips - strings, special chars, etc.
* Form with valid ips, but start instead of end and the opposite

## Deployment

In order to run the React App, we would:
cd scadefence-client &&
yarn start

## Built With

* [axios] - Requests to API
* [styled-components] - Application styling

## Acknowledgments

* The task was challenging but satisfying