import React from 'react';
import styled from "styled-components";
import IPRangeItem from "../../Components/List/IPRangeItem";
import PropTypes from "prop-types";


const IPRangeContainer = styled.div`
    background-color: white;
    width: 30%;
    margin: 0 auto;
    padding: 1vw;
    text-align: left;
    -webkit-box-shadow:inset 0px 0px 0px 1px #efefef;
    -moz-box-shadow:inset 0px 0px 0px 1px #efefef;
    box-shadow:inset 0px 0px 0px 1px #efefef;
    

	@media (max-width: 768px) {
        width:100%;
  }
`;
const IPRangesList = (props) => {

	const iprangesList = props.iprangesList;

	return (
		<IPRangeContainer>
			{iprangesList.map((singleIPRange, index) => (
				<IPRangeItem key={index} iprange={singleIPRange}/>
			))}
		</IPRangeContainer>
	);
};

// Set the required props
IPRangesList.propTypes = {
	iprangesList: PropTypes.array.isRequired,
};

export default IPRangesList;
