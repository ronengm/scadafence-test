import React from 'react';
import styled, {css} from 'styled-components';
import axios from "axios";
import PropTypes from "prop-types";

// Config settings
import Config from '../../Config';

// IPRange form stlying
const FormContainer = styled.div`
    background-color: #efefef;
    width: 30%;
    margin: 0 auto;
    padding: 1vw;
    
    
	@media (max-width: 768px) {
        width:100%;
        padding: 0;

  }
`;
const SubmitContainer = styled.div`
	text-align: right;
`;


const FormLabel = styled.label`
    display:block;
    text-align: left;
  }
`;

const FormError = styled.label`
    color:red;
  }
`;

const inputStyle = css`
  width:100%;
  margin-bottom:10px;
  padding: 0.6rem;
  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
  -moz-box-sizing: border-box;    /* Firefox, other Gecko */
  box-sizing: border-box;         /* Opera/IE 8+ */
  font-size: 0.8rem;

`;

const FormInput = styled.input`
  ${inputStyle}
`;
const FormSubmitButton = styled.input`
  text-align: center;
  background-color: #46a9dd;
  color: white;
  border-radius:4px;
  padding: 6px;
  font-size: 0.8rem;
`;
const SuccessMessage = styled.h1`
  color: green;
  float:left;
  font-size: 0.8rem;
`;


class IPRangesForm extends React.Component {

	// The state keeps all of the form variables
	constructor(props) {
		super(props);
		this.state = {
			ipStart: '',
			ipStartError: '',
			ipEnd: '',
			ipEndError: '',
			ipServerError: '',
			formSentSuccess: false,
			isInternal: false
		};

	}

	// On start IP range typing, update the state
	handleStartChange = (event) => {
		this.setState({
			ipStart: event.target.value
		});
	};

	// On end IP range, update the state
	handleEndChange = (event) => {
		this.setState({
			ipEnd: event.target.value
		});
	};

	// On ip range type, update the state
	handleInternalChange = (event) => {
		this.setState(prevState => ({
			isInternal: !prevState.isInternal
		}));
	};

	// Regexp that check if the ip from the user input is correct and valid
	isIPValid(ipaddress) {
		if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
			return (true);
		}
		return (false);
	}

	// IP addresses are simply string representations of 32bit numeric values.
	// By converting the string representation back into its numeric value we are able to compare them

	ipToNum(ip) {
		let parts = ip.split('.').map(function (str) {
			return parseInt(str);
		});

		return (parts[0] ? parts[0] << 24 : 0) +
			(parts[1] ? parts[1] << 16 : 0) +
			(parts[2] ? parts[2] << 8 : 0) +
			parts[3];
	};

	isArranged(ipStart, ipEnd) {
		return (this.ipToNum(ipStart) <= this.ipToNum(ipEnd));
	}


	// On submit - check if all values in the form are valid
	validateForm = () => {

		this.setState({
			formSentSuccess: false,
			ipStartError: '',
			ipEndError: ''
		});

		let isValid = true;
		const ipStart = this.state.ipStart;
		const ipEnd = this.state.ipEnd;


		if (!this.isIPValid(ipStart)) {
			isValid = false;
			this.setState({ipStartError: 'Start IP isn\'t valid'});
		} else {
			this.setState({ipStartError: ''});
		}
		if (!this.isIPValid(ipEnd)) {
			isValid = false;
			this.setState({ipEndError: 'End IP isn\'t valid'});
		} else {
			this.setState({ipEndError: ''});
		}

		if (
			this.isIPValid(ipStart) &&
			this.isIPValid(ipEnd) &&
			!this.isArranged(ipStart, ipEnd)
		) {
			isValid = false;
			this.setState({ipEndError: 'Initial IP must be smaller than ending IP'});
		}


		return isValid;

	};

	// On form submit, send the new iprange to the api
	handleSubmit = (event) => {
		event.preventDefault();
		if (this.validateForm()) {

			// Organize new IP Object
			const newIPRangeObj = {
				ipstart: this.state.ipStart,
				ipend: this.state.ipEnd,
				isinternal: this.state.isInternal,
			};

			// Send new ip range to the server
			axios.post(`${Config.apiURL}/ipranges`, newIPRangeObj)
				.then(res => {
					this.setState({formSentSuccess: true});
					this.props.updateList(res.data);
				}).catch(error => {
				this.setState({ipServerError: error.response.data.errorMessage});

			});
		}
	};


	render() {

		// State variables
		const {formSentSuccess, isInternal, ipStart, ipEnd, ipStartError, ipEndError, ipServerError} = this.state;

		// Class functions
		const {handleStartChange, handleEndChange, handleSubmit} = this;

		return (
			<FormContainer>
				<form onSubmit={handleSubmit}>
					<FormLabel>
						<FormInput type="text" placeholder={"Start: __.__.__.__"} value={ipStart}
						           onChange={handleStartChange} required/>
						<FormError>{ipStartError}</FormError>
					</FormLabel>
					<FormLabel>
						<FormInput placeholder={"End: __.__.__.__"} value={ipEnd}
						           onChange={handleEndChange} required/>
						<FormError>{ipEndError}</FormError>
					</FormLabel>
					<FormLabel><input type="checkbox" value={isInternal} onChange={this.handleInternalChange}/>Internal</FormLabel>
					<FormError>{ipServerError}</FormError>

					{formSentSuccess && <SuccessMessage>IPRange added successfully</SuccessMessage>}
					<SubmitContainer>
						<FormSubmitButton type="submit" value="Submit"/>
					</SubmitContainer>
				</form>
			</FormContainer>
		);
	}
}

// Set the required props
IPRangesForm.propTypes = {
	updateList: PropTypes.func.isRequired
};

export default IPRangesForm;
